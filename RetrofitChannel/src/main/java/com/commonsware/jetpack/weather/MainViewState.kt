/*
  Copyright (c) 2019-2020 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.

  Covered in detail in the book _Elements of Kotlin Coroutines_

  https://commonsware.com/Coroutines
*/

package com.commonsware.jetpack.weather

data class Scenario(val office: String, val gridX: Int, val gridY: Int)

sealed class MainViewState {
  object Loading : MainViewState()
  data class Content(val scenario: Scenario, val forecasts: List<RowState>) :
    MainViewState()

  data class Error(val throwable: Throwable) : MainViewState()
}
